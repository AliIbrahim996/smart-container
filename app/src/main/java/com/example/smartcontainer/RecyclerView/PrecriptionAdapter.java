package com.example.smartcontainer.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.smartcontainer.PreSettings;
import com.example.smartcontainer.R;
import com.example.smartcontainer.model.Prescription;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PrecriptionAdapter extends RecyclerView.Adapter<PrecriptionAdapter.PrescriptionViewHolder> {
    private Context mContext;
    private List<Prescription> prescriptions;
    private List<Prescription> prescriptions2;
    SharedPreferences sharedPreferences;
    String pre_id;
    String con_id;
    String email;
    String full_name;
    int pos;
    Parcelable reState;
    FirebaseDatabase database;

    public PrecriptionAdapter(Context mContext, String email, SharedPreferences
            sharedPreferences, String full_name, List<Prescription> prescriptions) {
        this.mContext = mContext;
        this.email = email;
        this.sharedPreferences = sharedPreferences;
        database = FirebaseDatabase.getInstance();
        this.full_name = full_name;
        this.prescriptions = prescriptions;
    }

    @NonNull
    @Override
    public PrescriptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View mView = inflater.inflate(R.layout.prescription_item, parent, false);
        return new PrescriptionViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull final PrescriptionViewHolder holder, final int position) {
        holder.prescriptionId.setText(prescriptions.get(position).getId());
        holder.containerId.setText(prescriptions.get(position).getContainer_id());
        pre_id = holder.prescriptionId.getText().toString();
        con_id = holder.containerId.getText().toString();
        pos = position;

        holder.preSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Todo open setting form(connect_disconnect) from user containers
                Intent intent = new Intent(mContext, PreSettings.class);
                intent.putExtra("pre_id", prescriptions.get(position).getId());
                intent.putExtra("email", email);
                intent.putExtra("con_id", prescriptions.get(position).getContainer_id());
                intent.putExtra("full_name", full_name);
                mContext.startActivity(intent);
                notifyDataSetChanged();
            }
        });

        holder.delPre.setOnClickListener(v -> {
            //Todo delete prescription
            database.getReference("Prescription/" + pre_id).removeValue()
                    .addOnSuccessListener(aVoid -> {
                        System.out.println("Container_id " + con_id);
                        if (!TextUtils.isEmpty(con_id)) {
                            database.getReference("Container/").child(con_id).child("prescriptionId")
                                    .setValue("").addOnSuccessListener(aVoid1 -> {
                                database.getReference("Container/").child(con_id)
                                        .child("contentTablesCount").setValue("")
                                        .addOnSuccessListener(aVoid2 -> {
                                            checkReminders();
                                            prescriptions.remove(pos);
                                            notifyDataSetChanged();
                                            Toast.makeText(mContext, "Prescription deleted successfully!",
                                                    Toast.LENGTH_LONG).show();


                                        });

                            });
                        } else {
                            checkReminders();
                            prescriptions.remove(pos);
                            notifyDataSetChanged();
                            notifyDataSetChanged();
                            Toast.makeText(mContext, "Prescription deleted successfully!",
                                    Toast.LENGTH_LONG).show();


                        }
                    });

        });
    }


    @Override
    public int getItemCount() {
        return prescriptions.size();
    }

    public static class PrescriptionViewHolder extends RecyclerView.ViewHolder {
        TextView containerId, prescriptionId, medName;
        ImageButton preSettings, delPre;
        int position;

        public PrescriptionViewHolder(@NonNull View itemView) {
            super(itemView);
            containerId = itemView.findViewById(R.id.conId);
            prescriptionId = itemView.findViewById(R.id.preId);
            medName = itemView.findViewById(R.id.medName);
            preSettings = itemView.findViewById(R.id.preSettings);
            delPre = itemView.findViewById(R.id.removePre);
            position = getAdapterPosition();
        }
    }

    private void checkReminders() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference("Reminder/");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                collectData((HashMap<Object, Object>) snapshot.getValue());
                System.out.println("On data change done!");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void collectData(HashMap<Object, Object> value) {
        List<String> arrayList = new ArrayList<>();
        System.out.println("value: " + value);
        System.out.println("**Fetching firebase data**/n/n");
        for (Object key : value.keySet()) {
            String reminder_id = key.toString();
            HashMap<String, Object> mdata = (HashMap<String, Object>) value.get(key);
            System.out.println(mdata);
            String prescriptionId = mdata.get("prescriptionId").toString();
            if (prescriptionId.equals(pre_id)) {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                database.getReference("Reminder/").child(reminder_id).removeValue();
            }

        }
    }

}
