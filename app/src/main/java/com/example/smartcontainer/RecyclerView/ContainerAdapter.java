package com.example.smartcontainer.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.smartcontainer.R;
import com.example.smartcontainer.model.Container;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class ContainerAdapter extends RecyclerView.Adapter<ContainerAdapter.ContainerViewHolder> {
    private Context mContext;
    String email;
    private List<Container> containers;
    SharedPreferences sharedPreferences;
    String container_id;
    int pos;
    FirebaseDatabase database;


    public ContainerAdapter(Context mContext, List<Container> containers, String email, SharedPreferences sharedPreferences) {
        this.mContext = mContext;
        this.containers = containers;
        this.email = email;
        this.sharedPreferences = sharedPreferences;
        database = FirebaseDatabase.getInstance();
    }

    @NonNull
    @Override
    public ContainerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View mView = inflater.inflate(R.layout.container_item, parent, false);
        return new ContainerViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ContainerViewHolder holder, int position) {
        holder.prescriptionId.setText(containers.get(position).getPrescriptionId());
        holder.containerId.setText(containers.get(position).getId());
        holder.medName.setText(containers.get(position).getContentTablesCount());
        holder.setCon_id(containers.get(position).getId());
        setPosition(position);
        holder.delCon.setOnClickListener(v -> {
            //Todo delete Container
            container_id = holder.getCon_id();
            database.getReference("Container/").
                    child(container_id).removeValue().addOnSuccessListener(
                    new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            if (!TextUtils.isEmpty(containers.get(position).getPrescriptionId())) {
                                database.getReference("Prescription/").
                                        child(containers.get(position).getPrescriptionId()).
                                        child("container_id").setValue("").addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(mContext, "Container deleted!", Toast.LENGTH_LONG)
                                                .show();
                                        containers.remove(pos);
                                       notifyDataSetChanged();
                                    }
                                });
                            } else {
                                Toast.makeText(mContext, "Container deleted!", Toast.LENGTH_LONG)
                                        .show();
                                containers.remove(pos);
                                notifyDataSetChanged();


                            }
                        }
                    }
            );

        });
    }

    private void setPosition(int position) {
        this.pos = position;
    }

    @Override
    public int getItemCount() {
        return containers.size();
    }


    public static class ContainerViewHolder extends RecyclerView.ViewHolder {
        TextView containerId, prescriptionId, medName;
        ImageButton delCon;
        String con_id;

        public ContainerViewHolder(@NonNull View itemView) {
            super(itemView);
            containerId = itemView.findViewById(R.id.con_Id);
            prescriptionId = itemView.findViewById(R.id.pre_Id);
            medName = itemView.findViewById(R.id.tab_count);
            delCon = itemView.findViewById(R.id.removeCon);
        }

        public String getCon_id() {
            return con_id;
        }

        public void setCon_id(String con_id) {
            this.con_id = con_id;
        }
    }
}
