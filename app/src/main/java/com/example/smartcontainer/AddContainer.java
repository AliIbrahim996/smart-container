package com.example.smartcontainer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.smartcontainer.model.Container;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddContainer extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    String email, full_name;

    String conId;
    EditText containerId;
    Context mContext;
    Activity activity;
    Toolbar myBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_container);
        Intent intent = getIntent();
        sharedPreferences = getSharedPreferences("Preferences", MODE_PRIVATE);
        email = intent.getStringExtra("email");
        full_name = intent.getStringExtra("full_name");
        myBar = findViewById(R.id.myAppBar);
        setSupportActionBar(myBar);
        getSupportActionBar().setTitle("");
        myBar.findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backPress();
            }
        });
        //Todo get fields property and send it as json object
        containerId = findViewById(R.id.container_id_edit);
        conId = containerId.getText().toString();
        mContext = getApplicationContext();
        activity = this;
    }

    @Override
    public void onBackPressed() {
        backPress();
    }

    private void backPress() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("Email", email);
        intent.putExtra("full_name", full_name);
        startActivity(intent);
        this.finish();
    }


    public void addCon(View view) {
        conId = containerId.getText().toString();
        if (TextUtils.isEmpty(conId)) {
            Toast.makeText(getApplicationContext(), "please fill the id", Toast.LENGTH_LONG).show();
            return;
        }
        //todo validate onId
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference("Container/" + conId);
        Container c = new Container("", "", email);
        databaseReference.setValue(c).addOnSuccessListener(aVoid -> {
                    Toast.makeText(getApplicationContext(), "Added successful", Toast.LENGTH_LONG).show();
                    containerId.setText("");
                }
        );
    }

    public void cancelAdd(View view) {
        this.containerId.setText("");
    }
}