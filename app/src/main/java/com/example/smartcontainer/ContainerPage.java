package com.example.smartcontainer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.smartcontainer.RecyclerView.ContainerAdapter;
import com.example.smartcontainer.model.Container;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ContainerPage extends Fragment {

    private RecyclerView conRView;
    SharedPreferences sharedPreferences;
    FloatingActionButton addContainerfab;
    List<Container> containers;
    String email, full_name;
    Context mContext;
    ContainerAdapter containerAdapter;
    View mView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.final_containers_screen, container, false);
        mContext = mView.getContext();
        conRView = mView.findViewById(R.id.conRView);
        conRView.setHasFixedSize(true);
        conRView.setLayoutManager(new LinearLayoutManager(mView.getContext()));
        addContainerfab = mView.findViewById(R.id.addContainerfab);
        sharedPreferences = mView.getContext().getSharedPreferences("Preferences", mContext.MODE_PRIVATE);
        Bundle b = getArguments();
        containers = new ArrayList<>();
        getAllContainers();
        email = b.getString("email");
        full_name = b.getString("full_name");
        addContainerfab.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, AddContainer.class);
            intent.putExtra("email", email);
            intent.putExtra("full_name", full_name);
            startActivity(intent);
            getActivity().finish();
        });
        return mView;
    }

    private void getAllContainers() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference("Container/");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                collectData((HashMap<Object, Object>) snapshot.getValue());
                System.out.println("On data change done!");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void collectData(HashMap<Object, Object> value) {
        System.out.println("value: " + value);
        System.out.println("**Fetching firebase data**/n/n");
        try {
            containers = new ArrayList<>();
            for (Object key : value.keySet()) {
                String con_ID = key.toString();
                System.out.println("Value: " + value.get(key));
                if (value.get(key) == null) {
                    Toast.makeText(mContext, "No data found!", Toast.LENGTH_LONG).show();
                    break;
                }
                HashMap<String, Object> mdata = (HashMap<String, Object>) value.get(key);
                System.out.println("Data: " + mdata);
                String Content_tables_Coun = mdata.get("contentTablesCount") != null ?
                        mdata.get("contentTablesCount") + "" : "";
                String Prescription_id = mdata.get("prescriptionId") != null ?
                        mdata.get("prescriptionId") + "" : "";
                String user_name = mdata.get("user_name") != null ?
                        mdata.get("user_name") + "" : "";
                System.out.println("User name: " + user_name);
                System.out.println("Email: " + email);
                Container c = new Container(con_ID, Content_tables_Coun, Prescription_id, user_name);
                if (email.equals(user_name)) {
                    containers.add(c);
                }
            }
            containerAdapter = new ContainerAdapter(mContext, containers, email, sharedPreferences);
            conRView.setAdapter(containerAdapter);
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }
    }
}