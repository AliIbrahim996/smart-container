package com.example.smartcontainer.model;

public class Reminder {
    public String prescriptionId;
    public String end_date;
    public String consumption_period;
    public String consumption_time;
    public String container_id;
    public String expire_date;

    public Reminder(String end_date, String consumption_period, String consumption_time, String container_id, String expire_date) {
        this.end_date = end_date;
        this.consumption_period = consumption_period;
        this.consumption_time = consumption_time;
        this.container_id = container_id;
        this.expire_date = expire_date;
    }

    public String getPrescriptionId() {
        return prescriptionId;
    }

    public void setPrescriptionId(String prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getConsumption_period() {
        return consumption_period;
    }

    public void setConsumption_period(String consumption_period) {
        this.consumption_period = consumption_period;
    }

    public String getConsumption_time() {
        return consumption_time;
    }

    public void setConsumption_time(String consumption_time) {
        this.consumption_time = consumption_time;
    }

    public String getContainer_id() {
        return container_id;
    }

    public void setContainer_id(String container_id) {
        this.container_id = container_id;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }
}
