package com.example.smartcontainer.model;

public class Prescription {

    private String id;
    private String container_id;
    private String dose;
    private String drugBarcode;
    private String tabletsCount;
    private String user_email;

    public Prescription() {
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public Prescription(String id, String container_id, String dose, String drugBarcode, String tabletsCount) {
        this.id = id;
        this.container_id = container_id;
        this.dose = dose;
        this.drugBarcode = drugBarcode;
        this.tabletsCount = tabletsCount;
    }

    public Prescription(String id, String container_id, String dose, String drugBarcode, String tabletsCount, String user_email) {
        this.id = id;
        this.container_id = container_id;
        this.dose = dose;
        this.drugBarcode = drugBarcode;
        this.tabletsCount = tabletsCount;
        this.user_email = user_email;
    }

    public Prescription(String container_id, String dose, String drugBarcode, String tabletsCount) {
        this.container_id = container_id;
        this.dose = dose;
        this.drugBarcode = drugBarcode;
        this.tabletsCount = tabletsCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContainer_id() {
        return container_id;
    }

    public void setContainer_id(String container_id) {
        this.container_id = container_id;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getDrugBarcode() {
        return drugBarcode;
    }

    public void setDrugBarcode(String drugBarcode) {
        this.drugBarcode = drugBarcode;
    }

    public String getTablets_count() {
        return tabletsCount;
    }

    public void setTablets_count(String tablets_count) {
        tabletsCount = tablets_count;
    }
}
