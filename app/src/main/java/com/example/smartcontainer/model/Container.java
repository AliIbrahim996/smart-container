package com.example.smartcontainer.model;

public class Container {

    /***
     *  "contentTablesCount" : 5,
     *       "prescriptionId" : 1,
     *       "user_name" : "ali"
     * */
    private String id;
    private String contentTablesCount;
    private String prescriptionId;
    private String user_name;


    public Container(String contentTables_Count, String prescriptionId, String user_name) {
        contentTablesCount = contentTables_Count;
        this.prescriptionId = prescriptionId;
        this.user_name = user_name;
    }

    public Container(String id, String contentTablesCount, String prescriptionId, String user_name) {
        this.id = id;
        this.contentTablesCount = contentTablesCount;
        this.prescriptionId = prescriptionId;
        this.user_name = user_name;
    }

    public Container() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContentTablesCount() {
        return contentTablesCount;
    }

    public void setContentTablesCount(String contentTablesCount) {
        this.contentTablesCount = contentTablesCount;
    }

    public String getPrescriptionId() {
        return prescriptionId;
    }

    public void setPrescriptionId(String prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
}