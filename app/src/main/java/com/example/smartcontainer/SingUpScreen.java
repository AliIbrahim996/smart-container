package com.example.smartcontainer;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.smartcontainer.model.User;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Random;
import java.util.regex.Pattern;

public class SingUpScreen extends Activity {
    String fullName;
    String email;
    String password;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.final_signup_screen);
        sharedPreferences = getSharedPreferences("Preferences", getApplicationContext().MODE_PRIVATE);
    }

    public void gotoSingIn(View view) {
        //Todo open singin screen
        this.finish();
        startActivity(new Intent(getApplicationContext(), LogInScreen.class));
    }

    public void SingUp(View view) {
        //Todo  get fields and connect to api
        EditText userEmail = findViewById(R.id.edit_email);
        EditText userPassword = findViewById(R.id.edit_password);
        EditText userFullName = findViewById(R.id.edit_full_name);
        fullName = userFullName.getText().toString();
        email = userEmail.getText().toString();
        password = userPassword.getText().toString();
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "please Enter Email", Toast.LENGTH_LONG).show();
            return;
        }
        if (!isValidEmail(email)) {
            Toast.makeText(this, "please Enter Valid Email", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "please Enter Password", Toast.LENGTH_LONG).show();
            return;
        }
        if (!isVaildPassword(password)) {
            Toast.makeText(this, "please Enter Valid Password", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(fullName)) {
            Toast.makeText(this, "please Enter your fullName", Toast.LENGTH_LONG).show();
            return;
        }
        singUpFireBase();

    }
    private void singUpFireBase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        int userId = new Random().nextInt(150)*12;
        DatabaseReference databaseReference = database.getReference("User/"+ userId);

        User user = new User(fullName,email,password);
        databaseReference.setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getApplicationContext(),"Registerd succefuly",Toast.LENGTH_LONG).show();
            }
        });
    }
    public boolean isValidEmail(String email) {

        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +

                "[a-zA-Z0-9_+&*-]+)*@" +

                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +

                "A-Z]{2,7}$";


        Pattern pat = Pattern.compile(emailRegex);

        if (email == null)

            return false;

        return pat.matcher(email).matches();

    }

    public boolean isVaildPassword(String password) {
        String passRegex = "^[\\s\\S0-9]{8,32}$";
        Pattern pat = Pattern.compile(passRegex);
        if (password == null) {
            return false;
        }
        return pat.matcher(password).matches();
    }
}
