package com.example.smartcontainer;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public class LogInScreen extends AppCompatActivity {
    String password, email;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.final_login_screen);
        sharedPreferences = getSharedPreferences("Preferences", getApplicationContext().MODE_PRIVATE);
    }

    public void gotoSignUp(View view) {
        startActivity(new Intent(getApplicationContext(), SingUpScreen.class));
    }

    public void logIn(View view) {
        EditText userEmail = findViewById(R.id.user_email);
        EditText userPassowrd = findViewById(R.id.user_password);
        email = userEmail.getText().toString();
        password = userPassowrd.getText().toString();
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "please Enter Email", Toast.LENGTH_LONG).show();
            return;
        }
        if (!isValid(email)) {
            Toast.makeText(this, "please Enter Valid Email", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "please Enter password", Toast.LENGTH_LONG).show();
            return;
        }
        // LoginTask retrieveFeedTask = new LoginTask();
        //   retrieveFeedTask.execute();
        LogInTask();
    }

    private void LogInTask() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference("User/");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                collectData((HashMap<Object, Object>) snapshot.getValue());
                System.out.println("On data change done!");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void collectData(HashMap<Object, Object> value) {
        System.out.println("value: " + value);
        System.out.println("**Fetching firebase data**/n/n");
        for (Object key : value.keySet()) {
            System.out.println("Key : " + key);
            System.out.println("Value: " + value.get(key));
            HashMap<String,String> mdata = (HashMap<String, String>) value.get(key);
            System.out.println(mdata);
            String userEmail = mdata.get("email");
            String userPassword = mdata.get("password");
            String full_name = mdata.get("full_name");
            if(checkEquals(userEmail,userPassword)){
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("Email",email);
                intent.putExtra("full_name",full_name);
                Toast.makeText(getApplicationContext(),"Login successfully",Toast.LENGTH_LONG).show();
                startActivity(intent);
                finish();
            }
        }
    }

    private boolean checkEquals(String userEmail, String userPassword) {
        if(email.equals(userEmail) && password.equals(userPassword)){
            return  true;
        }
        else{
            return false;
        }
    }

    public boolean isValid(String email) {

        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +

                "[a-zA-Z0-9_+&*-]+)*@" +

                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +

                "A-Z]{2,7}$";


        Pattern pat = Pattern.compile(emailRegex);

        if (email == null)

            return false;

        return pat.matcher(email).matches();

    }
}