package com.example.smartcontainer;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.example.smartcontainer.model.Prescription;
import com.example.smartcontainer.model.Reminder;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.Random;


public class AddPrecription extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    String email, full_name;
    private static final int REQUEST_CAMERA_PERMISSION = 201;
    String start_date, end_date, alarm, duration;
    TextView start, end, dura, alar;
    CheckBox am, pm, h, d;
    Toolbar myBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_precription);
        Intent intent = getIntent();
        email = intent.getStringExtra("user_email");
        full_name = intent.getStringExtra("full_name");
        start = findViewById(R.id.start_date);
        end = findViewById(R.id.end_date);
        dura = findViewById(R.id.duration);
        alar = findViewById(R.id.alarm);
        am = findViewById(R.id.amCheck);
        pm = findViewById(R.id.pmCheck);
        h = findViewById(R.id.hours);
        d = findViewById(R.id.daysCheck);
        ImageButton add = findViewById(R.id.submitPreData);
        myBar = findViewById(R.id.myAppBar);
        setSupportActionBar(myBar);
        getSupportActionBar().setTitle("");
        myBar.findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backPress();
            }
        });
        final ImageButton cancel = findViewById(R.id.cancelPreData);
        sharedPreferences = getSharedPreferences("Preferences", MODE_PRIVATE);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (am.isChecked()) {
                    alarm = alar.getText() + " am";
                    pm.setChecked(false);
                }
                if (pm.isChecked()) {
                    alarm = alar.getText() + " pm";
                    am.setChecked(false);
                }
                if (h.isChecked()) {
                    duration = dura.getText() + " hours";
                    d.setChecked(false);
                }
                if (d.isChecked()) {
                    duration = dura.getText() + " days";
                    h.setChecked(false);
                }
                start_date = start.getText().toString();
                end_date = end.getText().toString();

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        emptyFileds();
                    }
                });
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                int preId = new Random().nextInt(100) * 12;
                DatabaseReference databaseReference = database.getReference("Prescription/" + preId);

                Prescription pre = new Prescription(preId + "", "", "", "", "", email);
                Reminder reminder = new Reminder(start_date, duration, alarm, "", end_date);
                reminder.setPrescriptionId(preId + "");
                databaseReference.setValue(pre).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        addReminder(reminder);
                    }
                });


                ImageButton qr = findViewById(R.id.qr_scan);
                ImageButton ocr = findViewById(R.id.ocr_scan);

                qr.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent camera_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(camera_intent, REQUEST_CAMERA_PERMISSION);
                    }
                });

                ocr.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent camera_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(camera_intent, REQUEST_CAMERA_PERMISSION);
                    }
                });


            }
        });
    }

    private void backPress() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("Email", email);
        intent.putExtra("full_name", full_name);
        startActivity(intent);
        this.finish();
    }

    private void addReminder(Reminder reminder) {
        int reminderId = new Random().nextInt(150) * 12;
        try {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = database.getReference("Reminder/" + reminderId);
            System.out.println(databaseReference);
            databaseReference.setValue(reminder).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(getApplicationContext(), "Added successfully!", Toast.LENGTH_LONG).show();
                    emptyFileds();
                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }


    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        //image
        Bitmap photo = (Bitmap) data.getExtras().get("data");
    }


    @Override
    public void onBackPressed() {
        backPress();
    }

    private void emptyFileds() {
        start.setText("");
        end.setText("");
        dura.setText("");
        alar.setText("");
        am.setChecked(false);
        pm.setChecked(false);
        h.setChecked(false);
        d.setChecked(false);
    }
}
