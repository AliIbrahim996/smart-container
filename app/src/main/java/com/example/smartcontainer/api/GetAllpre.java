package com.example.smartcontainer.api;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.smartcontainer.model.Prescription;

import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class GetAllpre {
    String user_email;
    List<Prescription> prescriptions;
    SharedPreferences sharedPreferences;
    Context mContext;

    public GetAllpre(String user_email, SharedPreferences sharedPreferences, Context context) {
        this.user_email = user_email;
        this.sharedPreferences=sharedPreferences;
        this.mContext = context;
    }


    public List<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(List<Prescription> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public void getAll(){
        GetAllTask retrieveFeedTask = new  GetAllTask();
        retrieveFeedTask.execute();
    }

    class  GetAllTask extends AsyncTask<Void, Void, String> {

        StringBuilder stringBuilder;
        String responseMessage;
        int responseCode;
        List<String> cookiesHeader;

        protected void onPreExecute() {
        }
        @Override
        protected String doInBackground(Void... voids) {
            try {
                URL url = new URL("http://"+
                        sharedPreferences.getString("SERVER_IP","192.168.1.13")+
                        "/smartContainer/API/getAllPrescriptions.php");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                System.out.println(url);
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(15000);
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                JSONObject uData= new JSONObject();
                uData.put("email",user_email);
                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(uData.toString());
                writer.flush();
                writer.close();
                os.close();
                urlConnection.connect();
                try {
                    System.out.println(urlConnection.getInputStream());
                    BufferedReader bufferedReader = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream()));
                    stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    responseCode=urlConnection.getResponseCode();
                    responseMessage=urlConnection.getResponseMessage();
                    System.out.println("Response Code: "+responseCode+"\nResponse Message :" + responseMessage);
                    return stringBuilder.toString();

                }
                finally{
                    urlConnection.disconnect();
                }
            }
            catch(Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }
        protected void onPostExecute(String response) {
            // Toast.makeText(LoginActivity.this, response,Toast.LENGTH_LONG).show();
            if(response == null) {
                Toast.makeText(mContext, "THERE WAS AN ERROR Response null",Toast.LENGTH_LONG).show();
                return;
            }
            if(responseCode !=200) {
                Toast.makeText(mContext, "THERE WAS AN ERROR response code is: "+responseCode,Toast.LENGTH_LONG).show();
                return;
            }
            // String email="", ,password="";
            String message="";
            String flag="",full_name;

            try {
                JSONObject jsonObject=new JSONObject(response);
                flag=jsonObject.getString("flag");
                message=jsonObject.getString("message");
                //JSONObject logIn_info= jsonObject.getJSONObject("LogIn_info");
                //  email=logIn_info.getString("email");
                // password=logIn_info.getString("password");

                if(flag.equals("1")){

                }
                else {

                }
            }catch (Exception e){

            }


        }
    }
}
