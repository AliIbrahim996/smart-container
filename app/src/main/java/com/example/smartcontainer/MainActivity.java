package com.example.smartcontainer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawer;
    private Context mContext;
    private ActionBarDrawerToggle mToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_screen);
        mContext = getApplicationContext();
        NavigationView nav = findViewById(R.id.nav_view);
        View header = nav.getHeaderView(0);
        Intent intent = getIntent();
        TextView name = header.findViewById(R.id.nav_FullName);
        TextView email = header.findViewById(R.id.nav_email);
        name.setText(intent.getStringExtra("full_name"));
        email.setText(intent.getStringExtra("Email"));
        drawer = findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(this, drawer, R.string.open, R.string.close);
        drawer.addDrawerListener(mToggle);
        mToggle.syncState();
        final Bundle b = new Bundle();
        b.putString("email", email.getText().toString());
        b.putString("full_name", name.getText().toString());
        nav.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_notification:
                        NotificationPage notificationPage = new NotificationPage();
                        notificationPage.setArguments(b);
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, notificationPage).
                                commit();
                        getSupportActionBar().setTitle("Notification");
                        break;
                    case R.id.nav_container:
                        //open container fragment
                        ContainerPage containerPage = new ContainerPage();
                        containerPage.setArguments(b);
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, containerPage).
                                commit();
                        getSupportActionBar().setTitle("ContainerPage");
                        break;
                    case R.id.nav_prescription:
                        //
                        PrescriptionPage prescriptionPage = new PrescriptionPage();
                        prescriptionPage.setArguments(b);
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, prescriptionPage).
                                commit();
                        getSupportActionBar().setTitle("PrescriptionPage");
                        break;
                    case R.id.nav_logOut:
                        startActivity(new Intent(getApplicationContext(), LogInScreen.class));
                        finish();
                        break;
                }
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (savedInstanceState == null) {

            PrescriptionPage prescriptionPage = new PrescriptionPage();
            prescriptionPage.setArguments(b);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment, prescriptionPage).commit();
            getSupportActionBar().setTitle("PrescriptionPage");
            nav.setCheckedItem(R.id.nav_prescription);
        }

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
