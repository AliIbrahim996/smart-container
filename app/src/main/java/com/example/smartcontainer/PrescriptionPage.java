package com.example.smartcontainer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.smartcontainer.RecyclerView.ContainerAdapter;
import com.example.smartcontainer.RecyclerView.PrecriptionAdapter;
import com.example.smartcontainer.api.GetAllpre;
import com.example.smartcontainer.model.Container;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.example.smartcontainer.model.Prescription;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PrescriptionPage extends Fragment {
    private RecyclerView preRView;

    SharedPreferences sharedPreferences;
    FloatingActionButton addPreFab;
    List<Prescription> prescriptions;
    Context mContext;
    Parcelable reState;
    PrecriptionAdapter precriptionAdapter;
    String email, full_name;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        final View mView = inflater.inflate(R.layout.final_perscription_screen, container, false);
        preRView = mView.findViewById(R.id.preRView);
        preRView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mView.getContext());
        preRView.setLayoutManager(linearLayoutManager);
        Bundle b = getArguments();
        email = b.getString("email");
        full_name = b.getString("full_name");
        addPreFab = mView.findViewById(R.id.addPreFab);

        sharedPreferences = mView.getContext().getSharedPreferences("Preferences", mView.getContext().MODE_PRIVATE);
        mContext = mView.getContext();
        getAllPre();
        addPreFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                Intent intent = new Intent(mView.getContext(), AddPrecription.class);
                intent.putExtra("user_email", email);
                intent.putExtra("full_name", full_name);
                startActivity(intent);
                getActivity().finish();
            }
        });//Todo getAll prescription
        return mView;
    }

    private void getAllPre() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference("Prescription/");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                collectData((HashMap<Object, Object>) snapshot.getValue());
                System.out.println("On data change done!");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void collectData(HashMap<Object, Object> value) {
        System.out.println("value: " + value);
        System.out.println("**Fetching firebase data**/n/n");
        try {
            prescriptions = new ArrayList<>();
            for (Object key : value.keySet()) {
                String preID = key.toString();
                System.out.println("Value: " + value.get(key));
                if (value.get(key) == null) {
                    Toast.makeText(mContext, "No data found", Toast.LENGTH_LONG).show();
                    break;
                } else {
                    HashMap<String, Object> mdata = (HashMap<String, Object>) value.get(key);
                    System.out.println(mdata);
                    String conID = mdata.get("container_id") != null ?
                            mdata.get("container_id") + "" : "";
                    String Dose = mdata.get("dose") != null ?
                            mdata.get("dose") + "" : "";
                    String Drug_barcode = mdata.get("drugBarcode") != null ?
                            mdata.get("drugBarcode") + "" : "";
                    String Tablets_count = mdata.get("tabletsCount") != null ?
                            mdata.get("tabletsCount") + "" : "";
                    String user_email = mdata.get("user_email") != null ?
                            mdata.get("user_email") + "" : "";

                    Prescription p = new Prescription(preID, conID, Dose, Drug_barcode, Tablets_count, user_email);
                    if (email.equals(user_email)) {
                        prescriptions.add(p);
                    }

                }
            }
            for (Prescription pre : prescriptions) {
                System.out.println("Data { " + pre.getId()
                        + "/ " + pre.getUser_email());
            }
            System.out.println("Size " + prescriptions.size());
            precriptionAdapter = new PrecriptionAdapter(mContext, email, sharedPreferences,
                    full_name, prescriptions);

            preRView.setAdapter(precriptionAdapter);


        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }
    }
}