package com.example.smartcontainer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PreSettings extends AppCompatActivity {
    String pre_id, old_Con;
    String con_id;
    String email, full_name, tab_count;
    EditText tablets_count;
    Spinner conSpinner;
    int pos;
    String reminderValue;
    SharedPreferences sharedPreferences;
    String flag;
    Toolbar myBar;
    Button refillBtn;
    TextView preText, conContainer;
    String text, oldTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.control);
        //get all containers for customer email and put them
        Intent intent = getIntent();
        pre_id = intent.getStringExtra("pre_id");
        old_Con = intent.getStringExtra("con_id");
        con_id = old_Con;
        email = intent.getStringExtra("email");
        full_name = intent.getStringExtra("full_name");
        conContainer = findViewById(R.id.conContainer);
        oldTxt = conContainer.getText().toString();
        text = oldTxt + "  " + old_Con;
        conContainer.setText(text);
        conSpinner = findViewById(R.id.container_spinner);
        sharedPreferences = getSharedPreferences("Preferences", MODE_PRIVATE);
        myBar = findViewById(R.id.myAppBar);
        setSupportActionBar(myBar);
        getSupportActionBar().setTitle("");
        preText = myBar.findViewById(R.id.preText);
        String txt = preText.getText() + " " + pre_id;
        preText.setText(txt);
        tablets_count = findViewById(R.id.tabletsCount);
        myBar.findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backPress();
            }
        });
        getAllCon();
        conSpinner.setOnItemSelectedListener(new CustomOnItemSelectListener());
        refillBtn = findViewById(R.id.refillBtn);
        refillBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "Tablets filled!", Toast.LENGTH_LONG).show();
                tab_count = tablets_count.getText().toString();
            }
        });

    }

    private void backPress() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("Email", email);
        intent.putExtra("full_name", full_name);
        old_Con = "";
        startActivity(intent);
        this.finish();
    }

    private void getAllCon() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference("Container/");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                collectData((HashMap<Object, Object>) snapshot.getValue());
                System.out.println("On data change done!");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void collectData(HashMap<Object, Object> value) {
        List<String> arrayList = new ArrayList<>();
        System.out.println("value: " + value);
        System.out.println("**Fetching firebase data**/n/n");
        for (Object key : value.keySet()) {
            String con_ID = key.toString();
            HashMap<String, Object> mdata = (HashMap<String, Object>) value.get(key);
            System.out.println(mdata);
            String user_name = mdata.get("user_name") + "";
            String prescriptionId = mdata.get("prescriptionId").toString();
            if (user_name.equals(email) && TextUtils.isEmpty(prescriptionId))
                arrayList.add(con_ID);
        }
        addItemToSpinner(arrayList);
    }

    public void updatePre(View view) {


        FirebaseDatabase databaseContainer = FirebaseDatabase.getInstance();
        FirebaseDatabase databasePrescription = FirebaseDatabase.getInstance();
        switch (flag) {
            case "connect":
                if (TextUtils.isEmpty(tab_count)) {
                    Toast.makeText(getApplicationContext(), "Pleas fill tablets count!", Toast.LENGTH_LONG).show();
                    return;
                }
                databaseContainer.getReference("Container/").child(con_id).child("prescriptionId")
                        .setValue(pre_id).addOnSuccessListener(aVoid ->
                        databaseContainer.getReference("Container/")
                                .child(con_id).child("contentTablesCount")
                                .setValue(
                                        tab_count).addOnSuccessListener(aVoid13 ->
                                databasePrescription.
                                        getReference("Prescription/").child(pre_id).
                                        child("container_id")
                                        .setValue(con_id).addOnSuccessListener(aVoid1 ->
                                        databasePrescription.getReference("Prescription/").child(pre_id).child("tabletsCount")
                                                .setValue(tab_count).addOnSuccessListener(aVoid11 -> {
                                            reminderValue = con_id;
                                            checkReminders();
                                            Toast.makeText(getApplicationContext(), "Connected successfully!",
                                                    Toast.LENGTH_LONG).show();
                                            flag = "";
                                            tablets_count.setText("");
                                            conContainer.setText(oldTxt + " " + con_id);
                                            old_Con = con_id;
                                            getAllCon();
                                        }))));
                break;
            case "disconnect":
                if (!TextUtils.isEmpty(old_Con)) {
                    databaseContainer.getReference("Container/").child(old_Con).child("prescriptionId")
                            .setValue("").addOnSuccessListener(aVoid ->
                            databaseContainer.getReference("Container/").child(old_Con).child("contentTablesCount")
                                    .setValue("").addOnSuccessListener(
                                    new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            databasePrescription.getReference("Prescription/").
                                                    child(pre_id).child("container_id")
                                                    .setValue("").addOnSuccessListener(aVoid12 -> {
                                                reminderValue = "";
                                                checkReminders();
                                                Toast.makeText(getApplicationContext(), "disConnected successfully!",
                                                        Toast.LENGTH_LONG).show();
                                                flag = "";
                                                tablets_count.setText("");
                                                conContainer.setText(oldTxt);
                                                getAllCon();
                                            });
                                        }
                                    }
                            )
                    );
                } else {
                    Toast.makeText(getApplicationContext(), "You should disconnect from the same container" +
                            "!", Toast.LENGTH_LONG).show();
                }

        }

    }

    public void cancel(View view) {
        Toast.makeText(getApplicationContext(), "Operation canceled!", Toast.LENGTH_LONG).show();
        flag = "";
    }

    public void connectFlag(View view) {
        Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_LONG).show();
        flag = "connect";

    }

    public void disConnectFlag(View view) {
        Toast.makeText(getApplicationContext(), "disConnected", Toast.LENGTH_LONG).show();
        con_id = "";
        flag = "disconnect";
    }

    class CustomOnItemSelectListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            con_id = conSpinner.getItemAtPosition(position).toString();
            System.out.println("Selected con " + con_id);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            con_id = "";
        }
    }

    private void addItemToSpinner(List<String> arrayList) {
        ArrayAdapter<String> typeAdapter;
        typeAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, arrayList);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        conSpinner.setAdapter(typeAdapter);
    }

    private void checkReminders() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference("Reminder/");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                collectReminderData((HashMap<Object, Object>) snapshot.getValue());
                System.out.println("On data change done!");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void collectReminderData(HashMap<Object, Object> value) {
        List<String> arrayList = new ArrayList<>();
        System.out.println("value: " + value);
        System.out.println("**Fetching firebase data**");
        for (Object key : value.keySet()) {
            String reminder_id = key.toString();
            HashMap<String, Object> mdata = (HashMap<String, Object>) value.get(key);
            System.out.println(mdata);
            String prescriptionId = mdata.get("prescriptionId").toString();
            if (prescriptionId.equals(pre_id)) {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                database.getReference("Reminder/")
                        .child(reminder_id)
                        .child("container_id").
                        setValue(reminderValue);
            }

        }
    }

    @Override
    public void onBackPressed() {
        backPress();
    }
}
